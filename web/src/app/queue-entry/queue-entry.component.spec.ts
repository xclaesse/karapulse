import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueueEntryComponent } from './queue-entry.component';

describe('QueueEntryComponent', () => {
  let component: QueueEntryComponent;
  let fixture: ComponentFixture<QueueEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueueEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueueEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
