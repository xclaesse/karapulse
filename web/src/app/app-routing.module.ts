import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProfileComponent} from './profile/profile.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {SearchComponent} from './search/search.component';
import {QueueComponent} from './queue/queue.component';
import {HistoryComponent} from './history/history.component';

const routes: Routes = [{path: 'profile', component: ProfileComponent},
  {
    path: 'dashboard', component: DashboardComponent, children: [
      {path: 'search', component: SearchComponent},
      {path: 'queue', component: QueueComponent},
      {path: 'history', component: HistoryComponent},
    ]
  },
  {path: '', redirectTo: 'dashboard/search', pathMatch: 'full'}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
