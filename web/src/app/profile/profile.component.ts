import {Component, Input, OnInit} from '@angular/core';
import {Profile} from '../profile';
import {UserService} from '../user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  @Input() profile: Profile = new Profile();

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.profile = this.userService.profile;
  }

  save() {
    this.userService.save();
  }
}
