import {AfterViewInit, Component, Inject, Input, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {UserService} from '../user.service';

export interface DialogData {
  name: string;
}

@Component({
  selector: 'app-first-run-dialog',
  templateUrl: './first-run-dialog.component.html',
  styleUrls: ['./first-run-dialog.component.css']
})
export class FirstRunDialogComponent implements OnInit {
  @Input() name: string = "";

  constructor(public dialog: MatDialog, private userService: UserService) {
  }

  ngOnInit() {
    if (this.userService.profile.name === 'default') {
      setTimeout(() => this.openDialog());
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NameDialogComponent, {
      // width: '250px',
      data: {name: this.name},
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      this.name = result;
      this.userService.profile.name = result;
      this.userService.save();
    });
  }
}

@Component({
  selector: 'app-first-run-dialog-content',
  templateUrl: 'namedialog.html',
})
export class NameDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<NameDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
