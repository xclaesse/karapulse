import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {CurrentSong, QueueEntry} from '../status';
import {KaraokeService} from '../karaoke.service';
import {Subject, Subscription} from 'rxjs';
import {QueueEntryComponent} from '../queue-entry/queue-entry.component';
import {UserService} from '../user.service';
import {Song} from "../song";

@Component({
  selector: 'app-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.css']
})
export class QueueComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['card'];
  currentSong?: CurrentSong = undefined;
  poller: Subscription | undefined;
  refreshTrigger: Subject<void> | undefined;
  currentQueue: QueueEntry[] = [];
  position?: number;

  constructor(private bottomSheet: MatBottomSheet, private karaokeService: KaraokeService, private userService: UserService) {
  }

  ngOnInit() {
    this.karaokeService.statusEvent$.subscribe(status => {
       this.currentSong = status.current_song;
       this.currentQueue = status.queue;
       this.position = status.position;
    });
  }


  ngOnDestroy(): void {
    if (this.poller) {
      this.poller.unsubscribe();
    }
  }

  etaAsStr(eta: number): string {
    // Convert second as 12:13
    const second = eta % 60;
    const secondStr = `${second}`.padStart(2,'0');
    const minutes = Math.round(eta/60);

    return `${minutes}:${secondStr}`;
}

  refreshQueue(): void {
    if (this.refreshTrigger) {
      this.refreshTrigger.next();
    }
  }

  entryOfCurrentUser(row: { user: string; }): boolean {
    return row.user === this.userService.profile.name.trim();
  }

  openBottomSheet(row: { user: string; }): void {
    console.log(row);
    if(this.entryOfCurrentUser(row)) {
      this.bottomSheet.open(QueueEntryComponent, {data: row});
    }
  }
}
