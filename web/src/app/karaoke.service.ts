import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EMPTY, interval, merge, Observable, Subject, Subscription} from 'rxjs';
import {Song} from './song';
import {UserService} from './user.service';

import {environment} from '../environments/environment';
import {catchError, debounceTime, delay, distinctUntilChanged, filter, startWith, switchMap} from 'rxjs/operators';

import {PlayHistory, State, Status} from './status';
import {RequestStatus} from './requestStatus';

@Injectable({
  providedIn: 'root'
})
export class KaraokeService {
  private karaokeUrl = environment.wsUrl;
  poller: Subscription;
  refreshTrigger: Subject<void>;

  constructor(private http: HttpClient, private userService: UserService) {
    this.refreshTrigger = new Subject<void>();
    this.poller = merge(interval(500), this.refreshTrigger)
        .pipe(
            startWith(0),
            switchMap(() => this.status())
        ).subscribe(res => this.fireStatusUpdates(res));
  }

  private playingEventSource = new Subject<boolean>();
  playingEvent$ = this.playingEventSource.asObservable();

  private statusUpdatedEventSource = new Subject<Status>();
  statusEvent$ = this.statusUpdatedEventSource.asObservable();
  private playHistoryUpdatedEventSource = new Subject<PlayHistory>();
  playHistoryEvent$ = this.playHistoryUpdatedEventSource.asObservable();

  next(): Observable<void> {
    return this.http.get<void>(this.karaokeUrl + 'next');
  }

  pause(): Observable<void> {
    this.playingEventSource.next(false);
    return this.http.get<void>(this.karaokeUrl + 'pause');
  }

   restartSong(): Observable<void> {
    this.playingEventSource.next(false);
    return this.http.get<void>(this.karaokeUrl + 'restart-song');
  }

  play(): Observable<void> {
    this.playingEventSource.next(true);
    return this.http.get<void>(this.karaokeUrl + 'play');
  }

  search(terms: Observable<string>) {
    return terms.pipe(debounceTime(400))
      .pipe(distinctUntilChanged())
      .pipe(filter(value => value.length > 2))
      .pipe(switchMap(term => this.searchEntries(term)));
  }

  private searchEntries(terms : string): Observable<Song[] | RequestStatus> {
    // Combine the result Observable with observable for the request status so we can show a loading spinner
    // Not sure this is the best way but will do till we understand rxJS better
    // don't hesitate to rewrite it.

    const isLoadingObservable: Observable<RequestStatus> = new Observable((subscriber) => {
      subscriber.next({isLoading: true});
    });

    const requestObservable: Observable<Song[] | RequestStatus> =
      this.http.get<Song[]>(this.karaokeUrl + 'search/' + encodeURIComponent(terms))
        // uncomment to test spinner
        // .pipe(delay(100000))
        .pipe(catchError(error => {
          console.log(error);
          return new Observable<RequestStatus>((subscriber => {
            subscriber.next({isLoading: false});
          }));
        }));
    return merge(isLoadingObservable, requestObservable);
  }

  enqueue(song: Song): Observable<void> {
    const sanitizedName = encodeURIComponent(this.userService.profile.name.trim());
    return this.http.get<void>(this.karaokeUrl + 'enqueue_db/' + song.id + '/' + sanitizedName);
  }

  removeQueuedEntry(songId: string): Observable<void> {
    return this.http.get<void>(this.karaokeUrl + 'remove_queue/' + songId);
  }

   private status(): Observable<Status> {
    return this.http.get<Status>(this.karaokeUrl + 'status/')
      .pipe(catchError(error => {
        console.log(error);
        return EMPTY;
      }));
  }

  public playHistory(): Observable<PlayHistory> {
    return this.http.get<PlayHistory>(this.karaokeUrl + 'history/')
      .pipe(catchError(error => {
        console.log(error);
        return EMPTY;
      }));
  }

  private fireStatusUpdates(status: Status) {
    this.statusUpdatedEventSource.next(status);
    this.playingEventSource.next(status.state === State.PLAYING);
  }
}
