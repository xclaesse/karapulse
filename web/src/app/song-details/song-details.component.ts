import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {Song} from '../song';
import {KaraokeService} from '../karaoke.service';

@Component({
  selector: 'app-song-details',
  templateUrl: './song-details.component.html',
  styleUrls: ['./song-details.component.css']
})
export class SongDetailsComponent implements OnInit {

  constructor(private bottomSheetRef: MatBottomSheetRef<SongDetailsComponent>,
              @Inject(MAT_BOTTOM_SHEET_DATA) private song: Song,
              private karaokeService: KaraokeService) {
  }

  ngOnInit() {
  }

  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }

  play(event: MouseEvent) {
    this.bottomSheetRef.dismiss();
    this.karaokeService.enqueue(this.song).subscribe();

    event.preventDefault();
  }
}
