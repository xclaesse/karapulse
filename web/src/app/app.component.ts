import {Component, OnInit} from '@angular/core';
import {UserService} from './user.service';
import {Profile} from './profile';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit {
  profile: Profile = new Profile();
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.profile = this.userService.profile;
    if (this.profile.name === 'default') {

    }
  }
}
