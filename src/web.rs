// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
use glib::Sender;
use rocket::figment::Figment;
use rocket::fs::FileServer;
use std::net::{IpAddr, Ipv4Addr};
use std::path::Path;
use std::sync::Mutex;
use std::time::{Duration, Instant};
use tokio::sync::oneshot;
use tokio::time::timeout;

use anyhow::anyhow;
use rocket::config::Config;
use rocket::http::Method;
use rocket::serde::json::Json;
use rocket::{get, routes, Build, Rocket, State};
use rocket_cors::{AllowedHeaders, AllowedOrigins};

use crate::config;
use crate::db::DB;
use crate::karapulse::{Message, Reply, Song};
use crate::protocol;

type Result<T, E = rocket::response::Debug<anyhow::Error>> = std::result::Result<T, E>;

// The minimum of time between two valid 'next' request.
// This is used to prevent next collisions when different users want to next the same song.
pub const NEXT_IGNORE_WINDOW: Duration = Duration::from_secs(5);

struct Web {
    tx: Mutex<Sender<Message>>,
    db: Mutex<DB>,
    last_next: Mutex<Option<Instant>>,
}

impl Web {
    fn new(tx: Sender<Message>, db: DB) -> Web {
        Web {
            tx: Mutex::new(tx),
            db: Mutex::new(db),
            last_next: Mutex::new(None),
        }
    }
}

#[get("/play")]
fn play(web: &State<Web>) {
    let tx = web.tx.lock().unwrap();
    tx.send(Message::Play).unwrap();
}

#[get("/pause")]
fn pause(web: &State<Web>) {
    let tx = web.tx.lock().unwrap();
    tx.send(Message::Pause).unwrap();
}

#[get("/next")]
fn next(web: &State<Web>) {
    {
        let mut last = web.last_next.lock().unwrap();
        if let Some(last) = *last {
            if last.elapsed() < NEXT_IGNORE_WINDOW {
                debug!("Ignore consecutive 'next' requests");
                return;
            }
        }
        *last = Some(Instant::now());
    }
    let tx = web.tx.lock().unwrap();
    tx.send(Message::Next).unwrap();
}

#[get("/search/<fields>")]
fn search(web: &State<Web>, fields: String) -> Result<Json<protocol::SearchResponse>> {
    debug!("search: {}", fields);
    let songs = {
        let db = web.db.lock().unwrap();
        let songs = db.search(&fields)?;
        songs.into_iter().map(protocol::Song::from_db).collect()
    };

    let result = protocol::SearchResponse::new(songs);
    Ok(Json(result))
}

// FIXME: this is kind of hacky. We should break the protocol to:
// - rename this API 'enqueue'
// - pass the type or song instead of parsing the ID as a string
#[get("/enqueue_db/<id>/<user>")]
fn enqueue_db(web: &State<Web>, id: protocol::SongId, user: String) -> Option<()> {
    let song = match id {
        protocol::SongId::Db(id) => {
            debug!("query DB for song id {}", id);
            let db = web.db.lock().unwrap();
            db.find_song(id)
        }
    };

    match song {
        Ok(song) => {
            debug!("enqueue: '{} - {}' from {}", song.artist, song.title, user);

            let tx = web.tx.lock().unwrap();

            tx.send(Message::Enqueue {
                user,
                song: Song::Db(song),
            })
            .unwrap();
            Some(())
        }
        Err(..) => {
            warn!("cannot enqueue {} from {}: didn't find song", id, user);
            None
        }
    }
}

#[get("/remove_queue/<song_id>")]
fn remove_queue(web: &State<Web>, song_id: protocol::SongId) {
    let tx = web.tx.lock().unwrap();
    tx.send(Message::RemoveSong { song_id }).unwrap();
}

#[get("/status")]
async fn status(web: &State<Web>) -> Result<Json<protocol::StatusResponse>> {
    let rx = {
        let tx = web.tx.lock().unwrap();
        let (reply_tx, rx) = oneshot::channel();

        tx.send(Message::GetStatus {
            reply_tx: Some(reply_tx),
        })
        .unwrap();
        rx
    };

    let d = Duration::from_secs(1);
    match timeout(d, rx).await {
        Ok(Ok(Reply::Status { status })) => Ok(Json(status)),
        _ => {
            let msg = "status reply timeout";
            warn!("{}", msg);
            Err(rocket::response::Debug(anyhow!(msg)))
        }
    }
}

#[get("/history")]
fn history(web: &State<Web>) -> Result<Json<protocol::HistoryResponse>> {
    let history = {
        let db = web.db.lock().unwrap();
        db.list_history()?
    };

    let result = protocol::HistoryResponse::new(history);
    Ok(Json(result))
}

#[get("/history/<user>")]
fn history_for(web: &State<Web>, user: String) -> Result<Json<protocol::HistoryResponse>> {
    let history = {
        let db = web.db.lock().unwrap();
        db.history_for(&user)?
    };

    let result = protocol::HistoryResponse::new(history);
    Ok(Json(result))
}

#[get("/all")]
fn all(web: &State<Web>) -> Result<Json<Vec<protocol::Song>>> {
    debug!("all");
    let songs = {
        let db = web.db.lock().unwrap();
        let songs = db.list_songs()?;
        songs.into_iter().map(protocol::Song::from_db).collect()
    };

    Ok(Json(songs))
}

#[get("/restart-song")]
fn restart_song(web: &State<Web>) {
    debug!("restart song");
    let tx = web.tx.lock().unwrap();
    tx.send(Message::RestartSong).unwrap();
}

fn rocket(path: &Path, tx: Sender<Message>, db: DB) -> Rocket<Build> {
    let web = Web::new(tx, db);

    let allowed_origins = AllowedOrigins::all();

    let cors = rocket_cors::CorsOptions {
        allowed_origins,
        allowed_methods: vec![Method::Get].into_iter().map(From::from).collect(),
        allowed_headers: AllowedHeaders::some(&["Authorization", "Accept"]),
        allow_credentials: true,
        ..Default::default()
    }
    .to_cors()
    .unwrap();

    let mut config = Config::release_default();
    config.port = 1848;
    config.address = IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0));
    config.shutdown.ctrlc = false;
    let figment = Figment::from(config);

    rocket::custom(figment)
        .mount(
            "/api",
            routes![
                play,
                pause,
                next,
                search,
                enqueue_db,
                status,
                history,
                history_for,
                all,
                restart_song,
                remove_queue,
            ],
        )
        .mount("/", FileServer::from(path))
        .manage(web)
        .attach(cors)
}

pub async fn start_web(tx: Sender<Message>, db: DB) -> anyhow::Result<()> {
    let mut path = Path::new(config::PKGDATADIR).join("web");
    if !path.exists() {
        // look into sources
        path = Path::new("web").join("dist").join("karaoke-ng");
    }

    let path = path
        .canonicalize()
        .map_err(|e| anyhow!("Failed finding web directory ({}): {}", path.display(), e))?;

    let _rocket = rocket(&path, tx, db).launch().await?;

    Ok(())
}

impl<'r> rocket::request::FromParam<'r> for protocol::SongId {
    type Error = &'r str;

    fn from_param(param: &'r str) -> Result<Self, Self::Error> {
        param.parse().map_err(|_| param)
    }
}

#[cfg(test)]
mod test {
    use super::rocket;
    use crate::db::DB;
    use crate::karapulse;
    use crate::karapulse::{Message, Reply, Song};
    use crate::queue::Queue;
    use crate::{db, protocol};

    use glib::MainContext;
    use rocket::http::Status;
    use rocket::local::asynchronous::Client;
    use serde_json::{json, Value};
    use std::cell::RefCell;
    use std::env;
    use std::path::PathBuf;
    use std::rc::Rc;
    use std::sync::Arc;
    use std::time::Duration;

    struct Test {
        client: Arc<Client>,
        // Need to keep the context alive during the test duration
        context: MainContext,
        message: Rc<RefCell<Option<Message>>>,
    }

    impl Test {
        fn new(
            client: Arc<Client>,
            context: MainContext,
            message: Rc<RefCell<Option<Message>>>,
        ) -> Test {
            Self {
                client,
                context,
                message,
            }
        }

        async fn received_message(&self) -> Option<Message> {
            while self.message.borrow().is_none() {
                tokio::time::sleep(Duration::from_millis(100)).await;
                self.context.iteration(false);
            }
            self.message.replace(None)
        }

        async fn get_query(&self, query: &'static str) -> (Status, Option<String>) {
            let response = self.client.get(query).dispatch().await;
            (response.status(), response.into_string().await)
        }

        async fn get_no_reply(&self, query: &'static str) {
            let (status, response) = self.get_query(query).await;
            assert_eq!(status, Status::Ok);
            assert_eq!(response, None);
        }

        async fn get_json(&self, query: &'static str) -> Value {
            let (status, response) = self.get_query(query).await;
            assert_eq!(status, Status::Ok);
            serde_json::from_str(&response.unwrap()).unwrap()
        }
    }

    fn populate_db(db: &DB) {
        db.add_song(&PathBuf::from("file1.mp3"), "artist1", "title1", Some(10))
            .expect("Failed adding song");

        let songs = db.list_songs().expect("Failed listing songs");

        let song = &songs[0];
        let id = db
            .add_history_db(song, "Alice")
            .expect("Failed adding history");
        db.set_history_played(id).unwrap();
    }

    async fn setup() -> Test {
        let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let db = DB::new_memory().expect("Failed creating DB");

        populate_db(&db);

        let context = glib::MainContext::new();
        let guard = context.acquire().unwrap();

        let message = Rc::new(RefCell::new(None));
        let message_clone = message.clone();
        rx.attach(Some(&context), move |mut msg| {
            let message = &message_clone;

            if let Message::GetStatus { ref mut reply_tx } = msg {
                let reply_tx = reply_tx.take().unwrap();
                let mut queue = Queue::new();

                let song1 = db::Song {
                    rowid: 1,
                    path: "file1.mp3".to_string(),
                    artist: "artist1".to_string(),
                    title: "title1".to_string(),
                    length: Some(10),
                };
                queue.add("alice", Song::Db(song1), None).unwrap();

                let song2 = db::Song {
                    rowid: 2,
                    path: "file2.mp3".to_string(),
                    artist: "artist2".to_string(),
                    title: "title2".to_string(),
                    length: Some(15),
                };
                queue.add("bob", Song::Db(song2), None).unwrap();

                let current_song = queue
                    .next_item()
                    .map(|s| protocol::StatusResponseSong::new(s, None).unwrap());
                let queue = queue
                    .snapshot()
                    .into_iter()
                    .map(|s| protocol::StatusResponseSong::new(s, Some(5)).unwrap())
                    .collect();

                let status = protocol::StatusResponse::new(
                    karapulse::State::Playing,
                    current_song,
                    Some(5),
                    queue,
                );
                reply_tx
                    .send(Reply::Status { status })
                    .expect("Failed sending status reply");
            };

            message.replace(Some(msg));
            glib::Continue(true)
        });

        // Can't serve the actual web client as it may not be build
        let path = env::temp_dir();

        let client = Arc::new(
            Client::tracked(rocket(&path, tx, db))
                .await
                .expect("valid rocket instance"),
        );

        drop(guard);
        Test::new(client, context, message)
    }

    #[rocket::async_test]
    async fn play() {
        let test = setup().await;
        test.get_no_reply("/api/play").await;
        assert_eq!(test.received_message().await, Some(Message::Play));
    }

    #[rocket::async_test]
    async fn pause() {
        let test = setup().await;
        test.get_no_reply("/api/pause").await;
        assert_eq!(test.received_message().await, Some(Message::Pause));
    }

    #[rocket::async_test]
    async fn next() {
        let test = setup().await;
        test.get_no_reply("/api/next").await;
        assert_eq!(test.received_message().await, Some(Message::Next));
    }

    #[rocket::async_test]
    async fn search() {
        let test = setup().await;
        assert_eq!(test.get_json("/api/search/nope").await, json!([]));
        assert_eq!(
            test.get_json("/api/search/artist1").await,
            json!([ { "id": "db-1", "artist": "artist1", "title": "title1", "length": 10, "type": "cdg" } ]),
        );
    }

    #[rocket::async_test]
    async fn enqueue_db() {
        let test = setup().await;

        test.get_no_reply("/api/enqueue_db/db-1/test").await;
        assert_eq!(
            test.received_message().await,
            Some(Message::Enqueue {
                user: "test".to_string(),
                song: Song::Db(db::Song {
                    rowid: 1,
                    path: "file1.mp3".to_string(),
                    artist: "artist1".to_string(),
                    title: "title1".to_string(),
                    length: Some(10),
                })
            })
        );

        let (status, _response) = test.get_query("/api/enqueue_db/100/test").await;
        assert_eq!(status.code, 404);
    }

    #[rocket::async_test]
    async fn status() {
        let test = setup().await;
        let response = test.client.get("/api/status").dispatch();
        let message = test.received_message();
        let (response, _) = tokio::join!(response, message);
        let (status, response) = (response.status(), response.into_string().await);
        assert_eq!(status, Status::Ok);
        let reply: Value = serde_json::from_str(&response.unwrap()).unwrap();
        assert_eq!(
            reply,
            json!({ "current_song": {"song": { "artist": "artist1", "id": "db-1", "length": 10, "title": "title1", "type": "cdg" },
                "user": "alice"},
                "position": 5,
                "queue": [ {"song": { "artist": "artist2", "id": "db-2", "length": 15, "title": "title2", "type": "cdg" }, "user": "bob", "eta": 5 } ],
                "state": "Playing" })
        );
    }

    #[rocket::async_test]
    async fn history() {
        let test = setup().await;

        fn check_history(result: Value) {
            let result = result.as_array().unwrap();
            assert_eq!(result.len(), 1);
            let result = &result[0];
            assert_eq!(result["id"], 1);
            assert!(!result["played"].is_null());
            assert!(!result["queued"].as_str().unwrap().is_empty());
            assert_eq!(result["user"], "Alice");
            assert_eq!(
                result["song"],
                json!( { "id": "db-1", "artist": "artist1", "title": "title1", "length": 10, "type": "cdg" })
            );
        }

        check_history(test.get_json("/api/history").await);
        assert_eq!(test.get_json("/api/history/nope").await, json!([]));
        check_history(test.get_json("/api/history/Alice").await);
    }

    #[rocket::async_test]
    async fn all() {
        let test = setup().await;
        assert_eq!(
            test.get_json("/api/all").await,
            json!([ { "id": "db-1", "artist": "artist1", "title": "title1", "length": 10, "type": "cdg" } ]),
        );
    }

    #[rocket::async_test]
    async fn restart_song() {
        let test = setup().await;
        test.get_no_reply("/api/restart-song").await;
        assert_eq!(test.received_message().await, Some(Message::RestartSong));
    }
}
