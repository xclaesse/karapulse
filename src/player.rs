// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use crate::config;
use crate::karapulse::{Message, Song};
use anyhow::{anyhow, Result};
use get_if_addrs::get_if_addrs;
use gettextrs::*;
use glib::{Sender, SourceId};
use gstreamer as gst;
use gstreamer::prelude::*;
use gtk::prelude::*;
use std::cell::RefCell;
use std::path::Path;
use std::sync::{Arc, Mutex};
use url::Url;

pub struct Player {
    playbin: gst::Element,
    tx: Sender<Message>,
    announce_id: Arc<Mutex<RefCell<Option<SourceId>>>>,
}

fn get_overlay_text() -> String {
    // Ignore loopback and virtual interfaces
    // FIXME: don't hardcode port
    get_if_addrs()
        .unwrap()
        .iter()
        .filter(|iface| !iface.name.starts_with("virbr"))
        .filter(|iface| !iface.name.starts_with("vmnet"))
        .filter(|iface| !iface.name.starts_with("wg"))
        .filter(|iface| !iface.name.starts_with("docker"))
        .filter(|iface| !iface.name.starts_with("br-"))
        .map(get_if_addrs::Interface::ip)
        .filter(|ip| !ip.is_loopback())
        .filter(|ip| ip.is_ipv4())
        .map(|ip| format!("http://{}:1848", ip))
        .collect::<Vec<String>>()
        .join("\n")
}

impl Player {
    pub fn new(window: gtk::Window, tx: Sender<Message>, no_gl: bool) -> Result<Player> {
        let video_sink = if no_gl {
            gst::ElementFactory::make("gtksink")
                .build()
                .map_err(|_| anyhow!("Missing element gtksink"))?
        } else {
            let gtkglsink = gst::ElementFactory::make("gtkglsink")
                .build()
                .map_err(|_| anyhow!("Missing element gtkglsink"))?;
            let glsinkbin = gst::ElementFactory::make("glsinkbin")
                .build()
                .map_err(|_| anyhow!("Missing element glsinkbin"))?;
            glsinkbin.set_property("sink", &gtkglsink);

            glsinkbin
        };

        let playbin = gst::ElementFactory::make("playbin3")
            .build()
            .map_err(|_| anyhow!("Missing element playbin3"))?;

        playbin.set_property("video-sink", &video_sink);

        /* Window */
        let widget = video_sink.property::<gtk::Widget>("widget");

        window.add(&widget);

        let player = Player {
            playbin,
            tx: tx.clone(),
            announce_id: Arc::new(Mutex::new(RefCell::new(None))),
        };

        player.watch_bus(tx);

        Ok(player)
    }

    fn watch_bus(&self, tx: Sender<Message>) {
        let bus = self.playbin.bus().unwrap();
        bus.add_watch_local(move |_, msg| {
            match msg.view() {
                gst::MessageView::Eos(..) => {
                    debug!("Reached EOS");
                    tx.send(Message::PlayingDone).unwrap();
                }
                gst::MessageView::Error(err) => {
                    error!(
                        "Error from {:?}: {} ({:?})",
                        err.src().map(|s| s.path_string()),
                        err.error(),
                        err.debug()
                    );
                    tx.send(Message::PlayingDone).unwrap();
                }
                _ => (),
            };

            glib::Continue(true)
        })
        .expect("Failed to add bus watch");
    }

    fn start_pipeline(&self, media: &Path, video_filter: &str) -> Result<()> {
        debug!("opening {}", media.display());
        self.set_source(media)?;

        let filter = gst::parse_bin_from_description(video_filter, true)?;
        self.playbin.set_property("video-filter", &filter);

        self.play()
    }

    fn set_source(&self, media: &Path) -> Result<()> {
        self.playbin.set_state(gst::State::Null)?;

        let path = media.canonicalize()?;
        let uri = Url::from_file_path(path.to_str().unwrap()).unwrap();
        self.playbin.set_property("uri", &uri.to_string());

        let mut suburi: Option<String> = None;
        // Assume mp3 file have a CDG associated
        if let Some(ext) = media.extension() {
            if ext == "mp3" {
                let path = path.with_extension("cdg");
                let url = Url::from_file_path(path.to_str().unwrap()).unwrap();
                suburi = Some(url.to_string());
            }
        }
        self.playbin.set_property("suburi", &suburi);

        Ok(())
    }

    pub fn display_message(
        &self,
        message: &str,
        add_overlay_text: bool,
        timer: Option<u64>,
    ) -> Result<()> {
        let mut data_dir = Path::new(config::PKGDATADIR);
        if !data_dir.exists() {
            data_dir = Path::new("data");
        }

        let path = data_dir.join("background.png").canonicalize()?;

        let txt = if add_overlay_text {
            format!("{}\n{}", message, get_overlay_text())
        } else {
            message.to_string()
        };
        let txt = glib::markup_escape_text(&txt);

        let desc = format!("textoverlay text=\"{}\" shaded-background=true valignment=center halignment=center ! imagefreeze", txt);

        self.remove_announce_id();
        self.start_pipeline(&path, &desc)?;

        if let Some(timer) = timer {
            // setup timer to notify caller when the message is done being displayed
            let playbin_weak = self.playbin.downgrade();
            let tx = self.tx.clone();
            let announce_id_clone = self.announce_id.clone();

            let source_id = glib::source::timeout_add_seconds(1, move || {
                let mut announce_id = announce_id_clone.lock().unwrap();

                let playbin = match playbin_weak.upgrade() {
                    Some(playbin) => playbin,
                    None => {
                        announce_id.get_mut().take();
                        return glib::Continue(false);
                    }
                };

                if let Some(pos) = playbin.query_position::<gst::ClockTime>() {
                    if pos.seconds() < timer {
                        return glib::Continue(true);
                    }
                }

                trace!("display message done");
                tx.send(Message::DisplayMessageDone).unwrap();

                announce_id.get_mut().take();
                glib::Continue(false)
            });

            let mut announce_id = self.announce_id.lock().unwrap();
            announce_id.get_mut().replace(source_id);
        }

        Ok(())
    }

    pub fn open_song(&self, song: &Song) -> Result<()> {
        let desc = format!(
            "textoverlay text=\"{}\" shaded-background=true valignment=top halignment=right",
            get_overlay_text()
        );

        match song {
            Song::Path(path) => self.start_pipeline(path, &desc),
            Song::Db(song) => self.start_pipeline(&song.path(), &desc),
        }
    }

    fn set_state(&self, state: gst::State) -> Result<()> {
        debug!("set pipeline to {:?}", state);
        self.playbin.set_state(state)?;
        Ok(())
    }

    pub fn pause(&self) -> Result<()> {
        self.set_state(gst::State::Paused)
    }

    pub fn play(&self) -> Result<()> {
        self.set_state(gst::State::Playing)
    }

    pub fn stop(&self) -> Result<()> {
        debug!("Display background");
        self.display_message(gettext("Waiting for songs").as_str(), true, None)
    }

    pub fn get_position(&self) -> Option<u64> {
        self.playbin
            .query_position::<gst::ClockTime>()
            .map(|pos| pos.seconds())
    }

    fn destroy_pipeline(&self) {
        let bus = self.playbin.bus().unwrap();
        bus.remove_watch().unwrap();
        self.playbin.set_state(gst::State::Null).unwrap();
    }

    fn remove_announce_id(&self) {
        let announce_id = self.announce_id.lock().unwrap();

        if let Some(source_id) = announce_id.take() {
            source_id.remove();
        }
    }

    pub fn restart_song(&self) -> Result<()> {
        // seeking is not working with cdg/mp3 so restart the whole pipeline #yolo
        self.set_state(gst::State::Null)?;
        self.play()?;

        Ok(())
    }
}

impl Drop for Player {
    fn drop(&mut self) {
        self.remove_announce_id();
        self.destroy_pipeline();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::common::init;
    use crate::tests::TestMedia;
    use std::{thread, time};

    struct Test {
        player: Player,
        window: gtk::Window,
    }

    impl Test {
        fn new() -> Test {
            let _ = env_logger::try_init();
            init().unwrap();

            let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);

            let context = glib::MainContext::ref_thread_default();
            let _guard = context.acquire().unwrap();

            rx.attach(None, move |msg| {
                if let Message::PlayingDone = msg {
                    gtk::main_quit()
                }
                glib::Continue(true)
            });

            let window = gtk::Window::new(gtk::WindowType::Toplevel);

            let test = Test {
                player: Player::new(window.clone(), tx, false).unwrap(),
                window: window.clone(),
            };

            window.set_size_request(800, 600);
            window.show_all();

            test
        }

        fn open_media(&mut self, media: TestMedia) {
            self.player.open_song(&Song::Path(media.path())).unwrap();
        }

        fn run_until_eos(&self) {
            gtk::main();
        }
    }

    impl Drop for Test {
        fn drop(&mut self) {
            unsafe {
                self.window.destroy();
            }
        }
    }

    #[test]
    #[ignore]
    fn play_twice() {
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        assert_eq!(test.player.get_position(), None);
        test.run_until_eos();
        assert_eq!(test.player.get_position(), Some(2));

        test.open_media(TestMedia::Video2s);
        test.run_until_eos();
    }

    #[test]
    #[ignore]
    fn play_next() {
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        thread::sleep(time::Duration::from_millis(500));

        test.open_media(TestMedia::Video2_2s);
        test.run_until_eos();
    }

    #[test]
    #[ignore]
    fn pause() {
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        thread::sleep(time::Duration::from_millis(500));

        test.player.pause().unwrap();
        thread::sleep(time::Duration::from_millis(500));
        test.player.play().unwrap();
        test.run_until_eos();
    }

    #[test]
    #[ignore]
    fn stop() {
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        test.player.stop().unwrap();
    }

    #[test]
    #[ignore]
    fn play_cdg_alone() {
        let mut test = Test::new();

        test.open_media(TestMedia::Audio);
        test.run_until_eos();

        test.open_media(TestMedia::Audio);
        test.run_until_eos();
    }

    #[test]
    #[ignore]
    fn play_video_then_cdg() {
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        test.run_until_eos();

        test.open_media(TestMedia::Audio);
        test.run_until_eos();
    }

    #[test]
    #[ignore]
    fn play_cdg_then_video() {
        let mut test = Test::new();

        test.open_media(TestMedia::Audio);
        test.run_until_eos();

        test.open_media(TestMedia::Video2s);
        test.run_until_eos();
    }

    #[test]
    #[ignore]
    fn restart_song() {
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        thread::sleep(time::Duration::from_millis(500));

        test.player.restart_song().unwrap();
        thread::sleep(time::Duration::from_millis(500));
        test.player.play().unwrap();
        test.run_until_eos();
    }
}
