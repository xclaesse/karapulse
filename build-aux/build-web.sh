#!/bin/bash

export MESON_BUILD_ROOT="$1"
export MESON_SOURCE_ROOT="$2"
export NPM_CACHE="$3"

cd $MESON_SOURCE_ROOT/web/

if [ -z "$NPM_CACHE" ]
then
  npm install
else
  npm install --offline --cache=$NPM_CACHE
fi

./node_modules/.bin/ng build --output-hashing=none

cp dist/karaoke-ng/* $MESON_BUILD_ROOT/web/
