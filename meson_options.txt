option (
  'profile',
  type: 'combo',
  choices: [
    'default',
    'development'
  ],
  value: 'default'
)

option('cdg-plugin', type : 'boolean', value : true)
option('npm-cache', type : 'string', value : '')
option('offline', type : 'boolean', value : false)
